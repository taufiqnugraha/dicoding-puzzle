﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class gameController : MonoBehaviour {
    public List<Button> btns = new List<Button>();
    [SerializeField]
    private Sprite bgImage;

    [SerializeField]
    public Sprite[] puzzle;
    [SerializeField]
    GameObject win;
    [SerializeField]
    GameObject lose;
    public List<Sprite> gamePuzzles = new List<Sprite>();

    private bool firstGuess, secondGuess;

    private int countGuess;
    private int correctCountGuess;
    private int GameGuess;

    private string firstGuessPuzzle, secondnGuessPuzzle;

    private int firstGuessIndex, secondGuessIndex;
    [SerializeField]
    private Text txtScore;
    void Awake()
    {
        puzzle = Resources.LoadAll<Sprite>("Sprite");
    }
    void Start()
    {
        GetButtons();
        AddListener();
        addgamePuzzle();
        RandomFunction(gamePuzzles);
        GameGuess = gamePuzzles.Count / 2;
        win.SetActive(false);
        lose.SetActive(false);
    }
    void GetButtons()
    {
       
        GameObject[] objects = GameObject.FindGameObjectsWithTag("PuzzleButtons");

        for(int i = 0; i< objects.Length; i++)
        {
            
            btns.Add(objects[i].GetComponent<Button>());
            btns[i].image.sprite = bgImage;
        }
    }
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            Application.LoadLevel("Menu");
        }

    }
    void addgamePuzzle()
    {
        int looper = btns.Count;
        int index = 0;

        for(int i = 0; i< looper; i++)
        {
            if(index == looper/2)
            {
                index = 0;
            }
            gamePuzzles.Add(puzzle[index]);
            index++;
        }

    }
    void AddListener(){
        foreach(Button btn in btns)
        {
            btn.onClick.AddListener(() => PickUpPuzzle());

        }

    }
    public void PickUpPuzzle()
    {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;        
        Debug.Log("Hello! " + name);
        if(!firstGuess)
        {
            firstGuess = true;
            firstGuessIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name); 
            firstGuessPuzzle = gamePuzzles[firstGuessIndex].name;
            btns[firstGuessIndex].image.sprite = gamePuzzles[firstGuessIndex];

        }else if(!secondGuess){
            secondGuess = true;
            secondGuessIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name); 
            secondnGuessPuzzle = gamePuzzles[secondGuessIndex].name;
            btns[secondGuessIndex].image.sprite = gamePuzzles[secondGuessIndex];
            countGuess++;
            StartCoroutine(CheckIfThePuzzleMatch());
        
        }
    }
    IEnumerator CheckIfThePuzzleMatch()
    {
        yield return new WaitForSeconds(1f);
        if(firstGuessPuzzle == secondnGuessPuzzle){
            ContollerScript.Score += 30;
            txtScore.text = ContollerScript.Score.ToString();
            yield return new WaitForSeconds(.5f);
            btns[firstGuessIndex].interactable = false;
            btns[secondGuessIndex].interactable = false;

            btns[firstGuessIndex].image.color = new Color(0,0,0,0);
            btns[secondGuessIndex].image.color = new Color(0,0,0,0);    

            CheckIfTheGameMatch();
        }
        else
        {
            ContollerScript.Score -= 10;
            txtScore.text = ContollerScript.Score.ToString();
            yield return new WaitForSeconds(.5f);
            btns[firstGuessIndex].image.sprite = bgImage;
            btns[secondGuessIndex].image.sprite = bgImage;
        }
        yield return new WaitForSeconds(.5f);
        firstGuess = secondGuess = false;   
    }
    void CheckIfTheGameMatch()
    {
        correctCountGuess++;
        if(correctCountGuess == GameGuess)
        {

            Debug.Log("finish");
            if(ContollerScript.Score>=60){
            win.SetActive(true);
            }else
            {
                lose.SetActive(true);   
            }

            Debug.Log("Hello "+ countGuess );
        }
    }
    void RandomFunction(List<Sprite> list)
    {
        for(int i = 0; i< list.Count; i++)
        {
            Sprite temp = list[i];
            int randomIndex =Random.Range(i,list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }
    
}
