﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class gameOver : MonoBehaviour {
    public Text score;
	// Use this for initialization
	void Start () {
        score.text = ContollerScript.Score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Retry()
    {
        ContollerScript.Score = 0;
        SceneManager.LoadScene("lv1", LoadSceneMode.Single);
    }
}
