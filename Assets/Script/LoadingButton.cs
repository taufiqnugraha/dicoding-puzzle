﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingButton : MonoBehaviour {

    [SerializeField]
    private int scene;
   
    [SerializeField]
    private GameObject howtoplay;


    AsyncOperation async;


    IEnumerator IELoadNewScene()
    {
		yield return new WaitForSeconds (1);

        async = SceneManager.LoadSceneAsync(scene);

		while (!async.isDone) {
			yield return null;

        
        }
       

        
    }

    public void LoadingNewSceneButton()
    {
     
        howtoplay.SetActive(true);
        StartCoroutine(IELoadNewScene());
    }
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}